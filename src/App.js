import "./App.css";
import { GoogleMap, LoadScript } from "@react-google-maps/api";
import React from "react";
const containerStyle = {
  width: "1200px",
  height: "800px",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
};

const center = {
  lat: 10.787605355541865,
  lng: 106.7160635960529,
};

function App() {
  return (
    <div className="App">
      <LoadScript googleMapsApiKey="AIzaSyC3tF0MwOPKuG2Qn0x8qZn2bCNZa8z4Nho">
        <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={17}>
          {/* Child components, such as markers, info windows, etc. */}
          <></>
        </GoogleMap>
      </LoadScript>
    </div>
  );
}

export default App;
